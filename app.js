const express = require('express');
const path = require('path');
const pug = require('pug');
const router = require('./router');
const dotenv = require('dotenv');

const app = express();

app.set('view engine', 'pug');
app.set('views', 'views');

//using router file
app.use('/', router);

const public = process.env.STATIC || "public";
app.use(express.static('./'+ public));



// run port
dotenv.config({path: '.env'});
const port = process.env.PORT || 2000;
const server = app.listen(port, () => {
    console.log('Application running at port '+ server.address().port)
});
