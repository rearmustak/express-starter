const path = require('path');
const ExtractCss = require('extract-text-webpack-plugin');


module.exports = {
    entry: './public/app.js',
    output: {
        path: path.resolve(__dirname, 'public/dist'),
        filename: 'app.bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractCss.extract({
                        fallback: 'style-loader',
                        use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015']
                    }
                }
            }
        ]
    },
    plugins: [
        new ExtractCss('style.css')
    ]
}