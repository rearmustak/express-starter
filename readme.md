# Quickstart pack for Node Express@4 and Webpack@3

To install all packages from package.json file, run this code
```
npm install
```

To run server
```
npm run server
```
To run Webpack
```
npm run client
```
To run the full application
```
npm run start
```
